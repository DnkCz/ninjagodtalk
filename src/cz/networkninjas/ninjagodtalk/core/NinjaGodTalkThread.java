/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.networkninjas.ninjagodtalk.core;

import cz.networkninjas.ninjagodtalk.implementations.NinjaGodTalkInterface;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author DnkCz <DnkCz networkninjas.cz>
 */
public class NinjaGodTalkThread extends Thread {

    Socket socket;
    NinjaGodTalkInterface ninjaGodTalkInterface;
    Pattern GOD_RESPONSE;

    public NinjaGodTalkThread(Socket socket, NinjaGodTalkInterface ninjaGodTalkInterface) {
        this.socket = socket;
        this.ninjaGodTalkInterface = ninjaGodTalkInterface;
        GOD_RESPONSE = Pattern.compile("(God):[ \r\n]*[\\w]*");
    }

    @Override
    public void start() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream());
            String line, message;
            message = "";
            while ((line = in.readLine()) != null) {
                message += line;
            }

            if (message.length() > 0) {
                String answer = ninjaGodTalkInterface.getAnswer(message);
                if (answer != null && answer.length() > 0) {
                    System.out.println(answer);
                    // cut out only answer
                    Matcher matcher = GOD_RESPONSE.matcher(answer);
                    answer = matcher.group(1);
                    System.out.print(answer);
                } else {
                    System.out.print("Do not disturb now ! I´m working on ninja stuff.");
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(NinjaGodTalkThread.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }

    }

}
