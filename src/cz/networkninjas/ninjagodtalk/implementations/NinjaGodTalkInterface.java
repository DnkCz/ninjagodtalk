/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.networkninjas.ninjagodtalk.implementations;

import java.net.URL;

/**
 *
 * @author DnkCz <DnkCz networkninjas.cz>
 */
public interface NinjaGodTalkInterface {
    URL getURL();
    String getAnswer(String question);
}
