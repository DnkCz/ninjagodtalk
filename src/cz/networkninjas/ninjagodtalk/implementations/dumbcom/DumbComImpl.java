/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.networkninjas.ninjagodtalk.implementations.dumbcom;

import cz.networkninjas.ninjagodtalk.implementations.NinjaGodTalkInterface;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DnkCz <DnkCz networkninjas.cz>
 */
public class DumbComImpl implements NinjaGodTalkInterface {

    URL server_url;
    String CHARSET = StandardCharsets.UTF_8.name();

    public DumbComImpl() {
        try {
            initialize();
        } catch (MalformedURLException ex) {
            Logger.getLogger(DumbComImpl.class.getName()).log(Level.SEVERE, null, ex);
            this.server_url = null;
        }
    }

    @Override
    public URL getURL() {
        return this.server_url;
    }

    @Override
    public String getAnswer(String question) {
        if (server_url == null) {
            return "";
        }

        try {
            // setup connection properties
            URLConnection connection = server_url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Accept-Charset", CHARSET);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + CHARSET);
            connection.setRequestProperty("input", question);
            connection.setRequestProperty("Submit", "Submit");

            // get response
            InputStream response = connection.getInputStream();

            // get charset
            String contentType = connection.getHeaderField("Content-Type");
            String charset = null;
            for (String param : contentType.replace(" ", "").split(";")) {
                if (param.startsWith("charset=")) {
                    charset = param.split("=", 2)[1];
                    break;
                }
            }
            if (charset != null) {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(response, charset))) {
                    String line;
                    String message = "";
                    while ((line = reader.readLine()) != null) {
                        message += line;
                    }
                    return message;
                }
            } else {
                // It's likely binary content, use InputStream/OutputStream.
                return "";
            }

        } catch (IOException ex) {
            Logger.getLogger(DumbComImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    private void initialize() throws MalformedURLException {
        this.server_url = new URL("http://dumb.com/god/");

    }

}
