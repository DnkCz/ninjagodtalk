/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.networkninjas.ninjagodtalk.app;

import cz.networkninjas.ninjagodtalk.core.NinjaGodTalkThread;
import cz.networkninjas.ninjagodtalk.implementations.dumbcom.DumbComImpl;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DnkCz <DnkCz networkninjas.cz>
 */
public class NinjaGodTalk {

    private static final int SERVER_PORT = 1777;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(SERVER_PORT);
            while (true) {
               new NinjaGodTalkThread(serverSocket.accept(), new DumbComImpl()).start();
            }
        } catch (IOException ex) {
            Logger.getLogger(NinjaGodTalk.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
